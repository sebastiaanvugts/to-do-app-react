import React, { useCallback, useEffect, useState } from "react";
// import TodoList from "./todoList";

function Todo() {
  const [input, setInput] = useState("");
  const [todosList, setTodosList] = useState([]);
  const [todoEditing, setTodoEditing] = useState(null);
  const [editingText, setEditingText] = useState("");

  //laden van de data in localstorage
  useEffect(() => {
    const temp = localStorage.getItem("mijnLijstje");
    const loadedTodos = JSON.parse(temp);
    console.log("temp is: ", temp);

    if (loadedTodos) {
      setTodosList(loadedTodos);
    }
  }, []);
  //de array is leeg want hij hoeft maar 1x geladen te worden op de eerste page load

  const updateTodosList = (todos) => {
    localStorage.setItem("mijnLijstje", JSON.stringify(todos));
    setTodosList(todos);
  };

  const addToDo = useCallback(() => {
    const todos = [
      ...todosList,
      {
        content: input,
        id: Date.now(),
        completed: false,
      },
    ];

    updateTodosList(todos);
  }, [input, todosList]);

  const deleteTodo = useCallback(
    (id) => {
      const todos = todosList.filter((todo) => todo.id !== id);
      updateTodosList(todos);
    },
    [todosList]
  );

  //vragen of je een useState moet gebruiken voor de edit function

  function editTodoList(id, newContent) {
    console.log("ediiittteennn");
    const editMe = [...todosList].map((todo) => {
      if (id === todo.id) {
        return { ...todo, content: newContent };
      }
    });
    setTodosList(editMe);
  }

  function toggleComplete(id) {
    const updateTodo = [...todosList].map((todo) => {
      console.log("klikker de klak");
      if (todo.id === id) {
        todo.completed = !todo.completed;
      }
      return todo;
    });
    setTodosList(updateTodo);
  }

  function editTodo(id) {
    const editingUpdated = todosList.map((todo) => {
      if (todo.id === id) {
        todo.content = editingText;
      }
      return todo;
    });

    setTodosList(editingUpdated);
    setTodoEditing(null);
    setEditingText("");
  }

  return (
    <div className="App">
      <h1>React Todo App</h1>
      <input type="text" onChange={(e) => setInput(e.target.value)} />
      <button onClick={addToDo}>add todo</button>
      <div>
        <ul>
          {todosList.map(({ content, id }) => (
            <li key={id}>
              <input type="checkbox" onChange={() => toggleComplete(id)} />
              {todoEditing === id ? (
                <input
                  type="text"
                  onChange={(e) => setEditingText(e.target.value)}
                  value={editingText}
                />
              ) : (
                <span name={content} onDoubleClick={() => editTodoList(id)}>
                  {content}
                </span>
              )}
              <button onClick={() => deleteTodo(id)}>delete</button>
              {todoEditing === id ? (
                <button onClick={() => editTodo(id)}>Save Edit</button>
              ) : (
                <button onClick={() => setTodoEditing(id)}>Edit Me</button>
              )}
            </li>
          ))}
        </ul>
      </div>
    </div>
  );
}

export default Todo;
